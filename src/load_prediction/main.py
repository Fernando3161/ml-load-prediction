'''
Created on 04.07.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import pandas as pd
from src.load_prediction.common import DATA_DIR
from datetime import datetime, timedelta, time
import seaborn as sns
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame

# Load Data 
def get_data():
    historic_data=pd.read_csv(DATA_DIR+"\\Load_Data.csv")
    holidays_raw = pd.read_csv(DATA_DIR+"\\Holidays.csv")

    historic_data["Datetime"] = [datetime.strptime(dt[0:10], '%Y-%m-%d') for dt in list(historic_data["Date"])]


    # A bit of data exploration
    ## Fix the data base format
    historic_data["DatetimeHour"]= [date + timedelta(hours=hour) for date,hour in zip(historic_data["Datetime"],historic_data["Hour"]) ]
    historic_data.set_index("DatetimeHour", drop=True, inplace= True)
    
    ## Fix the holidays so they are a datetime object
    holidays = DataFrame()
    holidays["Date"]=[datetime.strptime(dt, '%d-%b-%Y') for dt in holidays_raw["Date"]]
    
    print(holidays)
    return historic_data, holidays


def explore_data(historic_data):
    historic_data_subset = historic_data.drop(["Date", "Datetime", "Hour"],axis=1)
    sns.pairplot(historic_data_subset)
    plt.show()
    
    return None


def generate_predictor_matrix(data,holidays):
    '''
    The function *genPredictors* generates the predictor variables used as
    inputs for the model. For short-term forecasting these include
    
    * Dry bulb temperature
    * Dew point
    * Hour of day
    * Day of the week
    * A flag indicating if it is a holiday/weekend
    * Previous day's average load
    * Load from the same hour the previous day
    * Load from the same hour and same day from the previous week
    
    If the goal is medium-term or long-term load forecasting, only the inputs
    hour of day, day of week, time of year and holidays can be used
    deterministically. The weather/load information would need to be
    specified as an average or a distribution

    
    :param data: A Dataset array of historical weather and load information
    :param holidays: A vector of holidays. 
    
    :return X: A matrix of predictor data where each row corresponds to an
                bservation (hourly load) and each column corresponds to a variable
    :return dates: A vector of dates for each observation
    :return labels: A cell array of strings describing each predictor
    '''
    
    # Convert dates into numeric values
    dates_nums= [int(dt.timestamp()/24/3600) for dt in data.index] 
    holidays_nums = [int(h.timestamp()/24/3600) for h in holidays["Date"]]

    print(holidays_nums)
    return None


# https://towardsdatascience.com/predict-daily-electric-consumption-with-neural-networks-8ba59471c1d
# https://towardsdatascience.com/using-neural-nets-to-predict-tomorrows-electric-consumption-cc1ae3ae7cc2

def main():
    historic_data, holidays = get_data()
    #explore_data(historic_data)
    generate_predictor_matrix(historic_data, holidays)

if __name__ == '__main__':
    main()
    pass