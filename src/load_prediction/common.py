'''
Created on 04.07.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import os

def get_project_root():
    """Return the path to the project root directory.

    :return: A directory path.
    :rtype: str
    """
    return os.path.realpath(os.path.join(
        os.path.dirname(__file__),
        os.pardir,os.pardir,
    ))


BASE_DIR = get_project_root()
DATA_DIR = BASE_DIR + "\\data"
RAW_DATA_DIR = DATA_DIR + "\\raw_data"
SOURCE_DIR = BASE_DIR + "\\src"
RESULTS_DIR = BASE_DIR + "\\results"

if __name__ == '__main__':
    print(BASE_DIR)
    pass