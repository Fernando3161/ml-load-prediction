## ML-Load Prediction

Instructive on how to implement ML algorithms for load and price prediction.


## Electricity Load Forecasting using Neural Networks

This example demonstrates building and validating a short term
electricity load forecasting model. The models take into
account multiple sources of information including temperatures and
holidays in constructing a day-ahead load forecaster. This script uses
Neural Networks.